using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovements : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("up"))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0,0,8);
        }
        if (Input.GetKeyDown("right"))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(5,0,0);
        }
        if (Input.GetKeyDown("down"))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0,0,-8);
        }
        if (Input.GetKeyDown("left"))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(-5,0,0);
        }
    }
}
