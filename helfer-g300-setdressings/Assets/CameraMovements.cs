using UnityEngine;

public class CameraMovements : MonoBehaviour
{
    public Transform target;

    public float smoothSpeed = 0.125f;
    public Vector3 offset;


    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = target.position + offset;
    }
}
